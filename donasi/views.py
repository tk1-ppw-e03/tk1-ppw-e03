from django.shortcuts import redirect, render
from .models import DonateModel
from . import forms
from django.http import HttpResponse
import random
import locale

# Create your views here.
def create(request):
    if request.method == 'POST':
        form = forms.DonateForm(request.POST)
        if form.is_valid():
            var = form.save()
            return redirect('donasi:payment',var.id)
    else:
        form = forms.DonateForm()
        return render(request, 'formDonate.html', {'form': form})


def payment(request,id):
    pay = DonateModel.objects.get(id=id)
    rdm = random.randint(1,99)
    total = pay.Jumlah+rdm
    rupiah = '{:20,}'.format(total)
    payy = '{:20,}'.format(DonateModel.objects.get(id=id).Jumlah)
    return render(request, 'payment.html', {'payy': payy, 'rdm':rdm, 'rupiah':rupiah})


def list_donate(request):
    donate=DonateModel.objects.all()
    return render(request, 'listDonasi.html',{'donate':donate})
