from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest, response
from .models import DonateModel
from .views import create, payment, list_donate

# Create your tests here.
class DonateTestEverything(TestCase):
    def test_url_donate(self):
        response = Client().get('/donate/')
        self.assertEquals(response.status_code, 200)
    
    def test_url_donate_payment(self):
        donate = DonateModel.objects.create(
            Nama = 'Astrid',
            Rekening = 'Astrid',
            Bank = 'BinkBunk',
            Jumlah = '10000',
            Pesan = 'ah mantap'
        )
        url = "/donate/payment/" + str(donate.id)+"/"
        response = Client().post(url)
        self.assertEquals(response.status_code, 200)

    def test_url_donate_list(self):
        response = Client().get('/donate/list/')
        self.assertEquals(response.status_code, 200)

    def test_template_formDonate(self):
        response = Client().get('/donate/')
        self.assertTemplateUsed(response, 'formDonate.html')

    def test_template_payment(self):
        donate = DonateModel.objects.create(
            Nama = 'Astrid',
            Rekening = 'Astrid',
            Bank = 'BinkBunk',
            Jumlah = '10000',
            Pesan = 'ah mantap'
        )
        url = "/donate/payment/" + str(donate.id)+"/"
        response = Client().get(url)
        self.assertTemplateUsed(response, 'payment.html')

    def test_template_listDonasi(self):
        response = Client().get('/donate/list/')
        self.assertTemplateUsed(response, 'listDonasi.html')
        
    def test_donate_using_create_func(self):
        found = resolve('/donate/')
        self.assertEqual(found.func, create)

    def test_donatepayment_using_payment_func(self):
        found = resolve('/donate/payment/1/')
        self.assertEqual(found.func, payment)

    def test_listDonate_using_list_donate_func(self):
        found = resolve('/donate/list/')
        self.assertEqual(found.func, list_donate)

    def test_model_DonateModel(self):
        DonateModel.objects.create(
            Nama = 'Astrid',
            Rekening = 'Astrid',
            Bank = 'BinkBunk',
            Jumlah = '10000',
            Pesan = 'ah mantap'
        )
        count = DonateModel.objects.all().count()
        self.assertEqual(count,1)
    
    def test_views_create(self):
        response = Client().post("/donate/", data={
            "Nama":"Astrid",
            "Rekening":"Astrid",
            "Bank":"BinkBunk",
            "Jumlah":"10000",
            "Pesan":"ah mantap"
        })
        count = DonateModel.objects.all().count()
        self.assertEqual(count,1)
        self.assertEqual(response.status_code,302)