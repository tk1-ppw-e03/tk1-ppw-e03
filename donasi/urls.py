from django.urls import path
from . import views

app_name = 'donasi'
urlpatterns = [
    path('', views.create, name='create'),
    path('payment/<int:id>/', views.payment, name='payment'),
    path('list/', views.list_donate, name='list_donate'),
]