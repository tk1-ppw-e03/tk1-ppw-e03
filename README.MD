# VIRUS PEPEW

## Pipeline Status
[![pipeline status](https://gitlab.com/tk1-ppw-e03/tk1-ppw-e03/badges/master/pipeline.svg)](https://gitlab.com/tk1-ppw-e03/tk1-ppw-e03/-/commits/master)

## Link Website
https://virus-pepew.herokuapp.com/

## Anggota Kelompok E03
* 1906292944 - Astrid Diany Alwardy
* 1906293316 - Shafira Putri Novia Hartanti
* 1906298771 - Arya Seputra Wibowo
* 1806147110 - Nur Wasi Na'wa
* 1906292995 - Cornelita Lugita Santoso

## Daftar Fitur
* Donasi - Astrid Diany Alwardy
* Data Rumah Sakit - Shafira Putri Novia Hartanti
* Screening - Arya Seputra Wibowo
* Question and Answer - Nur Wasi Na'wa
* Berita - Cornelita Lugita Santoso

## Stories
Selama 8 bulan terakhir, kita diharuskan menjaga diri di rumah agar terhindar dari sebuah wabah pandemi global yang baru muncul, virus Sars-CoV-2, atau yang biasa disebut virus Corona, virus yang memiliki bentuk protein seperti mahkota yang berfungsi untuk mengaitkan dirinya ke sel inangnya yang kemudian digunakan untuk menyalurkan RNA miliknya. PSBB-pun diberlakukan, banyak perkantoran dan sektor - sektor bisnis tutup. Banyak karyawan yang harus mengerjakan pekerjaan kantornya dari rumah, banyak juga pelajar yang melakukan kegiatan belajar mengajar dari rumah. Lalu ketika kasus mulai dapat dikendalikan, diberlakukanlah penerapan New Normal. Selama masa New Normal, kita diharuskan untuk terus menerapkan protokol kesehatan dan menjaga jarak satu sama lain ketika kita bepergian keluar rumah. 

Namun, seiring berjalannya penerapan New Normal, kasus COVID-19 di Indonesia malah semakin meningkat tak terkendali. Karena kejadian ini membuat kami resah yang akhirnya menggerakkan kami untuk membuat suatu website yang berisi informasi seputar COVID, QnA seputar COVID, form Screening, dan data rumah sakit serta donasi.

## Kebermanfaatan
* Donasi 

Memberikan bantuan berupa uang kepada yang membutuhkan seperti keluarga yang berdampak COVID, tenaga medis, dan garda terdepan penanganan COVID-19

* Data Rumah Sakit

Memberikan data rumah sakit rujukan penanganan COVID-19 sesuai dengan daerah pengguna

* Screening

Memberikan panduan tentang apa yang harus dilakukan pengguna tentang COVID-19

* Question and Answer

Pertanyaan dan diskusi seputar COVID-19

* Berita

Menyediakan berita - berita terupdate seputar COVID-19