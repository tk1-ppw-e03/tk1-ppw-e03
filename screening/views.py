from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from .models import Answer

# Create your views here.


def landing(request):
    return render(request, 'landing.html')


def tnc(request):
    return render(request, 'tnc.html')


def question(request):
    if request.method == 'POST':
        obj = Answer(age=request.POST.get("age"), test=request.POST.get(
            "test"), work=request.POST.get("work"))
        if request.POST.get("symptomDemam") != None:
            obj.is_Fever = True
        if request.POST.get("symptomSulitBernapas") != None:
            obj.is_HardBreathing = True
        if request.POST.get("symptomBatuk") != None:
            obj.is_Cough = True
        if request.POST.get("symptomMuntah") != None:
            obj.is_Vommitting = True
        if request.POST.get("symptomNyeri") != None:
            obj.is_Pain = True
        if request.POST.get("symptomNull") != None:
            obj.is_No = True

        if request.POST.get("Obesity") != None:
            obj.is_Obesity = True
        if request.POST.get("SmokeVape") != None:
            obj.is_SmokeVape = True
        if request.POST.get("Pregnant") != None:
            obj.is_Pregnant = True
        if request.POST.get("CirculatoryDisease") != None:
            obj.is_CirculatoryDisease = True
        if request.POST.get("BloodCellDisorder") != None:
            obj.is_BloodCellDisorder = True
        if request.POST.get("CondNull") != None:
            obj.is_CondNull = True
        obj.save()
        return HttpResponseRedirect('../../Screening/Result/')
    return render(request, 'questionnaire.html')


def result(request):
    context = Answer.objects.latest('id')
    sympList = context.Symptom
    condList = context.Condition
    return render(request, 'result.html', {'context': context, 'sympList': sympList, 'condList': condList, })
