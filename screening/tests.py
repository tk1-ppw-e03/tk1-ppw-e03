from http import HTTPStatus
from django.test import TestCase, Client
from django.urls import resolve
from .views import landing, tnc, question, result


# Create your tests here.
class ScreeningTest(TestCase):
    def test_screening_url_is_exist(self):
        response = Client().get('/Screening/')
        self.assertEqual(response.status_code, 200)

    def test_screening_url_is_using_landing(self):
        found = resolve('/Screening/')
        self.assertEqual(found.func, landing)

    def test_tnc_screening_url_is_exist(self):
        response = Client().get('/Screening/Privacy/')
        self.assertEqual(response.status_code, 200)

    def test_tnc_screening_url_is_using_landing(self):
        found = resolve('/Screening/Privacy/')
        self.assertEqual(found.func, tnc)
