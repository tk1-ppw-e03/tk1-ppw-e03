from django.urls import path
from .views import *

app_name = 'screening'
urlpatterns = [
    path('Screening/', landing),
    path('Screening/Privacy/', tnc),
    path('Screening/Questionnaire/', question),
    path('Screening/Result/', result),
]
