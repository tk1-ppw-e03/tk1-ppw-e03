from django.shortcuts import render, redirect
from .models import News
from .forms import CreateNews

# Create your views here.
def news_list(request):
	#return list of news
	news = News.objects.all().order_by('date')
	return render(request, "pages/news-list.html", {'news' : news})

def news_detail(request, id):
	#return detail of news
	item = News.objects.filter(pk = id)
	if item.exists():
		return render(request, "pages/news-detail.html", {'news' : item.first()})
	# return halaman error id not found
	return render(request, "pages/404.html", {'message' : 'Berita tidak ditemukan!'})

def news_create(request):
	#create new news
	form = CreateNews(request.POST or None)
	if request.method == 'POST' and form.is_valid():
		form.save()
		return redirect('news:news_list')
	return render(request, "pages/news-create.html", {'form' : form})
	