from django.db import models

# Create your models here.
class News(models.Model):
    title = models.CharField('Judul', max_length=255)
    source = models.CharField('Sumber', max_length=255)
    image_url = models.CharField('URL Gambar', max_length=255)
    content = models.TextField('Konten')
    date = models.DateTimeField(auto_now_add=True)
