from django.urls import path
from . import views

app_name = 'news'
urlpatterns = [
    path('list-news', views.news_list, name="news_list"),
    path('detail-news/<int:id>', views.news_detail, name="news_detail"),
    path('create-news', views.news_create, name="news_create"),
]