from http import HTTPStatus
from django.test import TestCase, Client
from django.urls import resolve
from .models import News
from .views import news_list, news_detail, news_create

# Create your tests here.
class NewsTest(TestCase):
    # Test URL
    def test_list_news_url_is_exist(self):
        response = Client().get('/list-news')
        self.assertEquals(response.status_code, HTTPStatus.OK)

    def test_list_news_using_news_list_func(self):
        found = resolve('/list-news')
        self.assertEqual(found.func, news_list)

    def test_detail_news_url_is_exist(self):
        new_news = News.objects.create(
            title = 'Judul Berita',
            source = 'Sumber Berita',
            image_url = 'Gambar Berita',
            content = 'Konten Berita'
        )
        my_url = "/detail-news/" + str(new_news.id)
        response = Client().get(my_url)
        self.assertEquals(response.status_code, HTTPStatus.OK)

    def test_detail_news_url_wrong_id(self):
        response = Client().get('/detail-news/1')
        self.assertEquals(response.status_code, HTTPStatus.OK)

    def test_detail_news_using_news_detail_func(self):
        found = resolve('/detail-news/1')
        self.assertEqual(found.func, news_detail)

    def test_create_news_url_is_exist(self):
        response = Client().get('/create-news')
        self.assertEquals(response.status_code, HTTPStatus.OK)

    def test_create_news_using_news_create_func(self):
        found = resolve('/create-news')
        self.assertEqual(found.func, news_create)

    # Test Model
    def test_create_news(self):
        News.objects.create(
            title = 'Judul Berita',
            source = 'Sumber Berita',
            image_url = 'Gambar Berita',
            content = 'Konten Berita'
        )
        counter = News.objects.all().count()
        self.assertEqual(counter, 1)

    # Test Views
    def test_post_news_form(self):
        response = self.client.post(
            "/create-news",
            data = {
                "title" : 'Judul Berita',
                "source" : 'Sumber Berita',
                "image_url" : 'Gambar Berita',
                "content" : 'Konten Berita'
            }
        )
        self.assertEqual(response.status_code, HTTPStatus.FOUND)
        counter = News.objects.all().count()
        self.assertEqual(counter, 1)

    def test_post_news_form_failed(self):
        response = self.client.post(
            "/create-news",
            data = {
                "source" : 'Sumber Berita',
                "image_url" : 'Gambar Berita',
                "content" : 'Konten Berita'
            }
        )
        self.assertEqual(response.status_code, HTTPStatus.OK)
        counter = News.objects.all().count()
        self.assertEqual(counter, 0)