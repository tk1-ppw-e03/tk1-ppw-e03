from django.apps import AppConfig


class PepewHomeConfig(AppConfig):
    name = 'pepew_home'
