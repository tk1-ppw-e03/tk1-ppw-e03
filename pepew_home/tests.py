from http import HTTPStatus
from django.test import TestCase, Client
from django.urls import resolve
from .views import home_view

# Create your tests here.
class HomeTest(TestCase):
    # Test Home
    def test_home_url_is_exist(self):
        response = Client().get('/')
        self.assertEquals(response.status_code, HTTPStatus.OK)

    def test_home_url_using_home_view(self):
        found = resolve('/')
        self.assertEqual(found.func, home_view)