from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest, response
from .models import QuestionModel
from .views import create, list_question

# Create your tests here.
class QuestionTestEverything(TestCase):
    def test_url_question(self):
        response = Client().get('/question/')
        self.assertEquals(response.status_code, 404)

    def test_url_question_list(self):
        response = Client().get('/question/list/')
        self.assertEquals(response.status_code, 404)
    
    def test_views_create(self):
        response = Client().post("/question/", data={
            "Nama":"Wasi",
			"Email":"nwasinawa@gmail.com",
            "Pesan":"....."
        })
        count = QuestionModel.objects.all().count()
        self.assertEqual(count,0)
        self.assertEqual(response.status_code,404)