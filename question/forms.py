from django import forms
from .models import QuestionModel

class QuestionForm(forms.ModelForm):
    class Meta:
        model = QuestionModel
        fields = ['Nama', 'Email', 'Pertanyaan']
        widgets = {
            'Nama': forms.TextInput(attrs={'placeholder': 'Masukkan nama Anda'}),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.Meta.fields:
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })
