from django.shortcuts import redirect, render
from .models import QuestionModel
from . import forms
from django.http import HttpResponse
import random

# Create your views here.
def create(request):
	form = forms.QuestionForm(request.POST or None)
	if request.method == 'POST' and form.is_valid():
		form.save()
		return redirect('question:list_question')
	return render(request, "formQuestion.html", {'form' : form})		
		
def list_question(request):
    question=QuestionModel.objects.all()
    return render(request, 'listQuestion.html',{'question':question})
