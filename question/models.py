from django.db import models

# Create your models here.
class QuestionModel(models.Model):
    Nama = models.CharField('Nama', max_length = 20)
    Email = models.EmailField('Email', max_length = 30)
    Pertanyaan = models.TextField()