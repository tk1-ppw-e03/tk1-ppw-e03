from django.urls import path
from . import views

app_name = 'rumah_sakit'

urlpatterns = [
    path('data-rumah-sakit/', views.dataRumahSakit, name='data-rumah-sakit'),
    path('data-rumah-sakit/add-rumah-sakit/', views.addRumahSakit, name='add-rumah-sakit'),
    path('data-rumah-sakit/detail-rumah-sakit/<int:pk>/', views.detailRumahSakit, name='detail-rumah-sakit'),
    path('data-rumah-sakit/remove-rumah-sakit/<int:pk>/', views.removeRumahSakit, name='remove-rumah-sakit')
    ]