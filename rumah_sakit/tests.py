from django.test import TestCase, Client
from django.urls import resolve

from .views import dataRumahSakit, addRumahSakit, detailRumahSakit, removeRumahSakit
from .models import RumahSakit

class RumahSakitTest(TestCase):

    # Test URL
    def test_dataRumahSakit_url_is_exist(self):
        response = Client().get('/data-rumah-sakit/')
        self.assertEqual(response.status_code, 200)

    def test_addRumahSakit_url_is_exist(self):
        response = Client().get('/data-rumah-sakit/add-rumah-sakit/')
        self.assertEqual(response.status_code, 200)

    def test_detailRumahSakit_url_is_exist(self):
        rumahSakit_baru = RumahSakit.objects.create(
            nama='Rumah Sakit A', 
            alamat='Jalan Melati Kav.70', 
            provinsi='DKI Jakarta', 
            telepon='02142424242', 
            fax='02188888888')
        response = Client().get('/data-rumah-sakit/detail-rumah-sakit/' + str(rumahSakit_baru.id) + '/')
        self.assertEqual(response.status_code, 200)

    def test_post_addRumahSakit_form_will_be_redirect(self):
        response = Client().post(
            "/data-rumah-sakit/add-rumah-sakit/",
            data = {
                "nama" : 'Rumah Sakit A',
                "alamat" : 'Jalan Melati Kav.70',
                "provinsi" : 'DKI Jakarta',
                "telepon" : '02142424242',
                "fax" : '02188888888'
            })
        self.assertEqual(response.status_code, 302)

    def test_removeRumahSakit_url_will_be_redirect(self):
        rumahSakit_baru = RumahSakit.objects.create(
            nama='Rumah Sakit A', 
            alamat='Jalan Melati Kav.70', 
            provinsi='DKI Jakarta', 
            telepon='02142424242', 
            fax='02188888888')
        response = Client().get('/data-rumah-sakit/remove-rumah-sakit/' + str(rumahSakit_baru.id) + '/')
        self.assertEqual(response.status_code, 302)

    #Test HTML Template
    def test_dataRumahSakit_using_dataRumahSakit_template(self):
        response = Client().get('/data-rumah-sakit/')
        self.assertTemplateUsed(response, 'rumah_sakit/dataRumahSakit.html')

    def test_addRumahSakit_using_addRumahSakit_template(self):
        response = Client().get('/data-rumah-sakit/add-rumah-sakit/')
        self.assertTemplateUsed(response, 'rumah_sakit/addRumahSakit.html')

    def test_detailRumahSakit_using_detailRumahSakit_template(self):
        rumahSakit_baru = RumahSakit.objects.create(
            nama='Rumah Sakit A', 
            alamat='Jalan Melati Kav.70', 
            provinsi='DKI Jakarta', 
            telepon='02142424242', 
            fax='02188888888')
        response = Client().get('/data-rumah-sakit/detail-rumah-sakit/' + str(rumahSakit_baru.id) + '/')
        self.assertTemplateUsed(response, 'rumah_sakit/detailRumahSakit.html')

    def test_dataRumahSakit_template_is_complete(self):
        response = Client().get('/data-rumah-sakit/')
        html_response = response.content.decode('utf8')
        self.assertIn('Tambah Rumah Sakit', html_response)
        self.assertIn('No', html_response)
        self.assertIn('Nama Rumah Sakit', html_response)
        self.assertIn('Provinsi', html_response)

    def test_addRumahSakit_template_is_complete(self):
        response = Client().get('/data-rumah-sakit/add-rumah-sakit/')
        html_response = response.content.decode('utf8')
        self.assertIn('Kembali', html_response)
        self.assertIn('Tambah Data Rumah Sakit', html_response)
        self.assertIn('Nama Rumah Sakit', html_response)
        self.assertIn('Alamat', html_response)
        self.assertIn('Provinsi', html_response)
        self.assertIn('No Telepon', html_response)
        self.assertIn('Fax', html_response)
        self.assertIn('Tambah', html_response)

    def test_detailRumahSakit_template_is_complete(self):
        rumahSakit_baru = RumahSakit.objects.create(
            nama='Rumah Sakit A', 
            alamat='Jalan Melati Kav.70', 
            provinsi='DKI Jakarta', 
            telepon='02142424242', 
            fax='02188888888')
        response = Client().get('/data-rumah-sakit/detail-rumah-sakit/' + str(rumahSakit_baru.id) + '/')
        html_response = response.content.decode('utf8')
        self.assertIn('Kembali', html_response)
        self.assertIn('Hapus', html_response)
        self.assertIn('Rumah Sakit A', html_response)
        self.assertIn('Jalan Melati Kav.70', html_response)
        self.assertIn('DKI Jakarta', html_response)
        self.assertIn('02142424242', html_response)
        self.assertIn('02188888888', html_response)
        
    #Test View
    def test_dataRumahSakit_using_dataRumahSakit_func(self):
        found = resolve('/data-rumah-sakit/')
        self.assertEqual(found.func, dataRumahSakit)

    def test_addRumahSakit_using_addRumahSakit_func(self):
        found = resolve('/data-rumah-sakit/add-rumah-sakit/')
        self.assertEqual(found.func, addRumahSakit)

    def test_detailRumahSakit_using_detailRumahSakit_func(self):
        rumahSakit_baru = RumahSakit.objects.create(
            nama='Rumah Sakit A', 
            alamat='Jalan Melati Kav.70', 
            provinsi='DKI Jakarta', 
            telepon='02142424242', 
            fax='02188888888')
        found = resolve('/data-rumah-sakit/detail-rumah-sakit/' + str(rumahSakit_baru.id) + '/')
        self.assertEqual(found.func, detailRumahSakit)

    def test_removeRumahSakit_using_removeRumahSakit_func(self):
        rumahSakit_baru = RumahSakit.objects.create(
            nama='Rumah Sakit A', 
            alamat='Jalan Melati Kav.70', 
            provinsi='DKI Jakarta', 
            telepon='02142424242', 
            fax='02188888888')
        found = resolve('/data-rumah-sakit/remove-rumah-sakit/' + str(rumahSakit_baru.id) + '/')
        self.assertEqual(found.func, removeRumahSakit)

    #Test Model
    def test_model_can_create_new_rumahSakit(self):
        rumahSakit_baru = RumahSakit.objects.create(
            nama='Rumah Sakit A', 
            alamat='Jalan Melati Kav.70', 
            provinsi='DKI Jakarta',
            telepon='02142424242', 
            fax='02188888888')
        jumlah_rumahSakit = RumahSakit.objects.all().count()
        self.assertEqual(jumlah_rumahSakit, 1)
        self.assertEqual(str(rumahSakit_baru), 'Rumah Sakit A')

