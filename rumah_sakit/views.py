from django.shortcuts import render, HttpResponseRedirect
from .models import RumahSakit
from .forms import AddRSForm

# Create your views here.
def dataRumahSakit(request):
    konteks = {
            'daftarRumahSakit': RumahSakit.objects.all()
        }
    return render(request, 'rumah_sakit/dataRumahSakit.html', konteks)

def addRumahSakit(request):
    konteks = {
            'form': AddRSForm()
        }
    if request.method == 'POST':
        form = AddRSForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/data-rumah-sakit/')
    return render(request, 'rumah_sakit/addRumahSakit.html', konteks)

def detailRumahSakit(request, pk):
    konteks = {
        'rumahSakit' : RumahSakit.objects.get(id=pk)
    }
    return render(request, 'rumah_sakit/detailRumahSakit.html', konteks)

def removeRumahSakit(request, pk):
    RumahSakit.objects.get(id=pk).delete()
    return HttpResponseRedirect('/data-rumah-sakit/')