from django.db import models

# Create your models here.
class RumahSakit(models.Model):

    PROVINSI= [ 
        ('Aceh', 'Aceh'),
        ('Sumatera Utara', 'Sumatera Utara'),
        ('Sumatera Barat', 'Sumatera Barat'),  
        ('Riau', 'Riau'),   
        ('Jambi', 'Jambi'),   
        ('Sumatera Selatan', 'Sumatera Selatan'), 
        ('Bengkulu', 'Bengkulu'),   
        ('Lampung', 'Lampung'),   
        ('Kep. Bangka Belitung', 'Kep. Bangka Belitung'),
        ('Kep. Riau', 'Kep. Riau'),
        ('DKI Jakarta', 'DKI Jakarta'),
        ('Jawa Barat', 'Jawa Barat'),
        ('Jawa Tengah', 'Jawa Tengah'),
        ('D.I. Yogyakarta', 'D.I. Yogyakarta'),
        ('Jawa Timur', 'Jawa Timur'),
        ('Banten', 'Banten'),
        ('Bali', 'Bali'),
        ('Nusa Tenggara Barat', 'Nusa Tenggara Barat'),
        ('Nusa Tenggara Timur', 'Nusa Tenggara Timur'),
        ('Kalimantan Barat', 'Kalimantan Barat'),
        ('Kalimantan Tengah', 'Kalimantan Tengah'),
        ('Kalimantan Selatan', 'Kalimantan Selatan'),
        ('Kalimantan Timur', 'Kalimantan Timur'),
        ('Kalimantan Utara', 'Kalimantan Utara'),
        ('Sulawesi Utara', 'Sulawesi Utara'),
        ('Sulawesi Tengah', 'Sulawesi Tengah'),
        ('Sulawesi Selatan', 'Sulawesi Selatan'),
        ('Sulawesi Tenggara', 'Sulawesi Tenggara'),
        ('Gorontalo', 'Gorontalo'),
        ('Sulawesi Barat', 'Sulawesi Barat'),
        ('Maluku', 'Maluku'),
        ('Maluku Utara', 'Maluku Utara'),
        ('Papua', 'Papua'),
        ('Papua Barat', 'Papua Barat')
    ]

    nama = models.CharField(max_length=50)
    alamat = models.TextField(max_length=200)
    provinsi = models.CharField(max_length=25, choices=PROVINSI)
    telepon = models.CharField(max_length=15)
    fax = models.CharField(max_length=15)

    def __str__(self):
        return self.nama
