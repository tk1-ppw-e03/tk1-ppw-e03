from django.forms import ModelForm
from django import forms

from .models import RumahSakit

class AddRSForm(ModelForm):
    class Meta:
        model = RumahSakit
        fields = '__all__'
        
        labels = {
            'nama': 'Nama Rumah Sakit', 
            'alamat': 'Alamat',
            'provinsi': 'Provinsi', 
            'telepon': 'No Telepon', 
            'fax': 'Fax', 
        }

        widgets = {
            'nama': forms.TextInput({'class':'form-control'}),
            'alamat': forms.TextInput({'class':'form-control'}),
            'provinsi': forms.Select({'class':'form-control'}), 
            'telepon': forms.TextInput({'class':'form-control'}), 
            'fax': forms.TextInput({'class':'form-control'})
        }

    def __init__(self, *args, **kwargs):
        kwargs.setdefault('label_suffix', '')  
        super(AddRSForm, self).__init__(*args, **kwargs)